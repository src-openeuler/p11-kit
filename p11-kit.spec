Name:           p11-kit
Version:        0.25.5
Release:        1
Summary:        Provides a way to load and enumerate PKCS#11 modules.
License:        BSD-3-Clause
URL:            https://p11-glue.freedesktop.org/p11-kit.html

Source0:        https://github.com/p11-glue/p11-kit/releases/download/%{version}/%{name}-%{version}.tar.xz
Source1:        p11-kit-client.service

BuildRequires: gcc
BuildRequires: meson >= 0.51
BuildRequires: pkgconfig(bash-completion)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(libffi) >= 3.0.0
BuildRequires: pkgconfig(libsystemd)
BuildRequires: pkgconfig(libtasn1) >= 2.3
BuildRequires: pkgconfig(systemd)
BuildRequires: gtk-doc
BuildRequires: /usr/bin/xsltproc
BuildRequires: /usr/bin/msgfmt

%description
Provides a way to load and enumerate PKCS#11 modules.
Provides a standard configuration setup for installing
PKCS#11 modules in such a way that they're discoverable.
Also solves problems with coordinating the use of PKCS#11
by different components or libraries living in the same process.

%package devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description devel
Provides header and libraries files for applications use %{name} to develop.

%package trust
Summary:        Trust policy module of %{name}
Requires:       %{name} = %{version}-%{release}
Requires(post): %{_sbindir}/update-alternatives
Requires(postun): %{_sbindir}/update-alternatives
Conflicts:      nss < 3.14.3-9

%description trust
This package contains PKCS#11 trust policy module.

%package server
Summary:        Server and client commands for %{name}
Requires:       %{name} = %{version}-%{release}

%description server
The %{name}-server package contains command line tools that enable to
export PKCS#11 modules through a Unix domain socket.  Note that this
feature is still experimental.

%package_help

%define multilib_arches ppc64 sparc64 x86_64 ppc64le
%ifarch %{multilib_arches}
%define alt_ckbi  libnssckbi.so.%{_arch}
%else
%define alt_ckbi  libnssckbi.so
%endif

%prep
%autosetup -p1

%build
%meson -Dman=true -Dgtk_doc=true -Dtrust_paths="/etc/pki/ca-trust/source:/usr/share/pki/ca-trust-source"
%meson_build

%install
%meson_install
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pkcs11/modules
install -p -m 644 %{S:1} $RPM_BUILD_ROOT%{_userunitdir}

%find_lang %{name}

# Install the example conf with %%doc instead
rm -f %{buildroot}%{_sysconfdir}/pkcs11/pkcs11.conf.example

%check
%meson_test

%post trust
%{_sbindir}/update-alternatives --install %{_libdir}/libnssckbi.so %{alt_ckbi} %{_libdir}/pkcs11/p11-kit-trust.so 30

%postun trust
if [ $1 -eq 0 ]; then
        %{_sbindir}/update-alternatives --remove %{alt_ckbi} %{_libdir}/pkcs11/p11-kit-trust.so
fi
 
%files -f %{name}.lang
%license COPYING
%doc %{_vpath_builddir}/p11-kit/pkcs11.conf.example
%dir %{_libexecdir}/p11-kit
%dir %{_sysconfdir}/pkcs11
%dir %{_sysconfdir}/pkcs11/modules
%dir %{_datadir}/p11-kit
%dir %{_datadir}/p11-kit/modules
%{_bindir}/p11-kit
%{_libdir}/libp11-kit.so.*
%{_libdir}/p11-kit-proxy.so
%{_libexecdir}/p11-kit/p11-kit-remote
%{_datadir}/bash-completion/completions/p11-kit

%files server
%{_libdir}/pkcs11/p11-kit-client.so
%{_userunitdir}/p11-kit-client.service
%{_libexecdir}/p11-kit/p11-kit-server
%{_userunitdir}/p11-kit-server.service
%{_userunitdir}/p11-kit-server.socket

%files help
%doc AUTHORS NEWS README
%{_mandir}/man1/trust.1*
%{_mandir}/man8/p11-kit.8*
%{_mandir}/man5/pkcs11.conf.5*

%files devel
%doc %{_datadir}/gtk-doc/*
%{_includedir}/p11-kit-1/
%{_libdir}/libp11-kit.so
%{_libdir}/pkgconfig/p11-kit-1.pc

%files trust
%{_bindir}/trust
%dir %{_libdir}/pkcs11
%ghost %{_libdir}/libnssckbi.so
%{_libdir}/pkcs11/p11-kit-trust.so
%{_datadir}/p11-kit/modules/p11-kit-trust.module
%{_libexecdir}/p11-kit/trust-extract-compat
%{_datadir}/bash-completion/completions/trust

%changelog
* Fri Aug 09 2024 Funda Wang <fundawang@yeah.net> - 0.25.5-1
- update to version 0.25.5
- build with meson instead

* Tue Sep 26 2023 wangyunjia <yunjia.wang@huawei.com> - 0.25.0-2
- backport patch for glib-networking

* Thu Jul 20 2023 wangyunjia <yunjia.wang@huawei.com> - 0.25.0-1
- update to 0.25.0

* Sun Jan 29 2023 zhengxiaoxiao <zhengxiaoxiao2@huawei.com> - 0.24.1-1
- update to 0.24.1

* Wed Jun 15 2022 duyiwei <duyiwei@kylinos.cn> - 0.24.0-2
- add subpackage server

* Wed Dec 1 2021 panxiaohe <panxiaohe@huawei.com> - 0.24.0-1
- update to 0.24.0

* Sat Jan 23 2021 panxiaohe <panxiaohe@huawei.com> - 0.23.22-1
- update to 0.23.22

* Sat Jan 9 2021 zoulin <zoulin13@huawei.com> - 0.23.20-2
- fix CVE-2020-29361 CVE-2020-29362 CVE-2020-29363

* Mon Jul 27 2020 Liquor <lirui130@huawei.com> - 0.23.20-1
- update to 0.23.20

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-6
- delete unused file

* Sat Dec 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-5
- fix update problem

* Fri Dec 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-4
- Add missing Requires

* Wed Nov 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-3
- Add Buildrequires: libxslt

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-2
- Correct patch number

* Sat Sep 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-1
- Package init
